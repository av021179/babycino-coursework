class TestBugJ2 {
    public static void main(String[] args) {
        int result;
	result = testFunction();
        System.out.println(result);
    }

    public static int testFunction() {
        int x;
	x = 5;
        int y;
	y = 10;
        return x += y += 3; // Bug: Assigning y to x instead of x + y
    }
}

